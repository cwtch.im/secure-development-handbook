# Message Formats

## Peer to Peer Messages

    PeerMessage {
        ID      string // A unique Message ID (primarily used for acknowledgments)
        Context string // A unique context identifier i.e. im.cwtch.chat
        Data    []byte // The context-dependent serialized data packet.
    }

### Context Identifiers

* `im.cwtch.raw` - Data contains a plain text chat message (see: [overlays](./overlays.md) for more information)
* `im.cwtch.acknowledgement` - Data is empty and ID references a previously sent message
  
* `im.cwtch.getVal` and `im.cwtch.retVal` - Used for requesting / returning specific information about a peer. Data
contains a serialized `peerGetVal` structure and `peerRetVal` respectively.
    
        peerGetVal struct {
                Scope string
                Path string
        }

        type peerRetVal struct {
            Val    string // Serialized path-dependent value
            Exists bool
        }

## Plaintext / Decrypted Group Messages

    type DecryptedGroupMessage struct {
        Text      string // plaintext of the message
        Onion     string // The cwtch address of the sender
        Timestamp uint64 // A user specified timestamp
        // NOTE: SignedGroupID is now a misnomer, the only way this is signed is indirectly via the signed encrypted group messages
        // We now treat GroupID as binding to a server/key rather than an "owner" - additional validation logic (to e.g.
        // respect particular group constitutions) can be built on top of group messages, but the underlying groups are
        // now agnostic to those models.
        SignedGroupID      []byte 
        PreviousMessageSig []byte // A reference to a previous message
        Padding            []byte // random bytes of length = 1800 - len(Text)
    }

DecryptedGroupMessage contains random padding to a fixed size that is equal to the length of all fixed length fields + 1800.
This ensures that all encrypted group messages are equal length.

## Encrypted Group Messages
    
    // EncryptedGroupMessage provides an encapsulation of the encrypted group message stored on the server
    type EncryptedGroupMessage struct {
        Ciphertext []byte
        Signature  []byte // Sign(groupID + group.GroupServer + base64(decrypted group message)) using the senders cwtch key
    }

Calculating the signature requires knowing the groupID of the message, the server the group is associated with
and the decrypted group message (and thus, the Group Key). It is (ed25519) signed by the sender of the message, and can be
verified using their public cwtch address key.