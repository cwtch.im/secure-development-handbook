# Key Bundles

Cwtch servers identify themselves through signed key bundles. These key bundles contain a list of keys necessary
to make cwtch group communication secure and metadata resistant.

At the time of writing, key bundles are expected to contain 3 keys:

1. A Tor v3 Onion Service Public Key for the Token Board (ed25519)- used to connect to the service over Tor to post and
receive messages.
2. A Tor v3 Onion Service Public Key for the Token Service (ed25519) - used to acquire tokens to post on the service via
a small proof-of-work exercise.
3. A Privacy Pass Public Key - used in the token acquisition process (a ristretto curve point) . See: [OPTR2019-01](https://openprivacy.ca/research/OPTR2019-01/)

The key bundle is signed and can be verified via the first v3 onion service key, thus binding it to that particular oninon
address.

## Verifying Key Bundles

Profiles who import server key bundles verify them using the following trust-on-first-use (TOFU) algorithm:

1. Verify the attached signature using the v3 onion address of the server. (If this fails, the import process is halted)
2. Check that every key type exists. (If this fails, the import process is halted)
3. If the profile has imported the server key bundle previously, assert that all the keys are the same. (If this fails, the import process is halted)
4. Save the keys to the servers contact entry.

In the future this algorithm will likely be altered to allow the addition of new public keys (e.g. to allow
tokens to be acquired via a Zcash address.)

Technically, at steps (2) and (3() the server can be assumed to be malicious, having signed a valid key bundle that
does not conform to the specifications. When groups are moved from "experimental" to "stable" such an action will
result in a warning being communicated to the profile.