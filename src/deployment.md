# Deployment


##  Risk: Binaries are replaced on the website with malicious ones

**Status: Partially-mitigated**

While this process is now mostly automated, should this automation ever be
compromised then there is nothing in our current process that would detect this.

We need:

* Reproducible Builds - we currently use public docker containers for all builds
  which should allow anyone to compare distributed builds with ones built from source.
 * Signed Releases - Open Privacy does not yet maintain a public record of staff
 public keys. This is likely a necessity for signing released builds and
  creating an audit chain backed by the organization. This process must be
  manual by definition.
  
