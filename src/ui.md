# Cwtch UI

The UI is now built using [flutter](https://flutter.dev/).

![](https://docs.openprivacy.ca/cwtch-security-handbook/3.png)

## Deanonymization through Content Injection

**Status: Mitigated in several places**

Unlike most UI frameworks, Flutter is not a de-facto HTML rendering engine. Any kind of malicious content
injection is therefore not-elevated to a critical deanonymization risk in the default case.

To further mitigate this risk:

* Maintain our own set of core UI widgets that the rest of the app relies on that do not
  make use of any component widgets that may hit the network e.g. [Image.network](https://api.flutter.dev/flutter/widgets/Image/Image.network.html)
* Mediate all Cwtch api networking calls through Tor
* Frequently test the UI for potential content injection vulnerabilities via FuzzBot

While none of these mitigations should be assumed robust by themselves, the
combination of them should be sufficient to prevent such attacks.


## Corruption of UI Chrome through Content Injection

**Status: Mitigated**

While we assess the mitigated-risk of content injection resulting in deanonymization vectors to be very low,
the risk that malicious content causes UI chrome corruption requires additional consideration.

As a simple example, unicode control characters from conversations should not result in corruption to parts of the 
chrome that they are rendered with.

![](./fuzzbot-invite.png)

To mitigate this risk:

* All potentially malicious content is rendered separately at the widget level i.e. we do not mix trusted
  strings and untrusted strings in the same widget. This confined rendering differences tightly to just the
  malicious content.
* Frequently test the UI for potential content injection vulnerabilities via FuzzBot

## Denial of Service through Spamming

**Status: Partially Mitigated**

There is currently no limitation on the number of messages that can be sent
to a Cwtch server or by a Cwtch peer. Each message requires process and is
added to the UI if valid.

We have put in work to ensure that an influx of messages does not degrade the
app experience, however it will result in an increase in network bandwidth which
may be intolerable or undesired for many people - especially those on metered
connections (e.g. cellphone data plans)

In order to be suitable to deploy groups at a wide scale, the app requires a way
to prevent Cwtch from fetching information over such connections, and this
should likely be turned on by default.


## Testing Status

The UI is subject to both manual testing, partially automated testing through fuzzbot, and fully
automated testing during pull requests.



