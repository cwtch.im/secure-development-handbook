# Risks

## Private information transiting the IPC boundary

**Status: Requires privileged user to exploit**

Information used to derive the encryption key used to save all sensitive data to
the file system cross the boundary between the UI front-end and the App backend.

Intercepting this information requires a privileged position on the local
machine.

## Testing Status

Cwtch features one [well-defined integration test](https://openprivacy.ca/discreet-log/06-cwtch-integ-tests/) which exercise the ideal case of
three well-formed peers authenticating and messaging each other through an
untrusted server.

Tests are run with the `-race` flag which will cause them to fail if
race conditions are detected.

Both integration tests and unit tests are run automatically for every pull request and main branch merge.

## Resolved or Outdated Risks



### Dependency on Outdated Protobuf Implementation

**Status: Mitigated**

The group features of Cwtch are enabled by an untrusted infrastructure
protcol that was originally implemented using the older ricochet-based channels. The go
code that was generated from these channels no longer works given the newest
version of the protobufs framework.

We have removed protobufs entirely from the project by porting this
functionality over the Tapir.

### Proof of Work (PoW) Spam Prevention as a Metadata Vector

**Status: Outdated**: Cwtch now uses Token Based Services to separate challenges like PoW from
resolving the tokens.

Processing capabilities are not constant, and so a malicious server could perform
some correlations/fiddle with difficulty per connection in an attempt to identify
connections over time.

Needs some statistical experimentation to quantify, but given the existing
research detecting timeskews over Tor I wouldn't be surprised if this could be derived.

As for mitigation: Adding a random time skew might be an option,some defense
against the server adjusting difficulty too often would also mitigate some of
the more extreme vectors.

Additionally, Token Based Services and Peer-based Groups are both potential
options for eliminating this attack vector entirely.

## Thread Safety

**Status: Mitigated**

The Cwtch library evolved from a prototype that had weak checks around
concurrency, and the addition of singleton behavior around saving profiles to
files and protocol engines resulted in race conditions.

The inclusion of the `Event Bus` made handling such cases easier, and the
code is now tested via unit tests and integration test
running the `-race` flag.

