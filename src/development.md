# Development

The main process to counter malicious actors in development of Cwtch is the 
openness of the process.

To enhance this openness, automated builds, testing and packaging are defined
as part of the repositories - improving te robustness of the code base at every
stage.

![](https://docs.openprivacy.ca/cwtch-security-handbook/1.png)

While individual tests aren't perfect, and all processes have gaps, we should be
committed to make it as easy as possible to contribute to Cwtch while also
 building pipelines and processes that catch errors (unintential or malicious)
 as soon as possible.

### Risk: Developer Directly Pushes Malicious Code

**Status: Mitigated** 

`trunk` is currently locked and only 3 Open Privacy staff members have permission
to override it, in addition the responsibility of monitoring changes.

Further every new pull request and merge triggered automated builds & tests
 which trigger emails and audit logs.
 
The code is also open source and inspectable by anyone.

### Risk: Code Regressions

**Status: Partially Mitigated** (See individual project entries in this
 handbook for more information)
 
Our automated pipelines have the ability to catch regressions when that
 behaviour is detectable.
 
The greatest challenge is in defining how such regressions are detected for the
[ui](./ui.md) - where behaviour isn't as strictly defined as it is for the
 individual libraries.
 