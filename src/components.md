# Component Ecosystem Breakdown

Cwtch is made up of several smaller component libraries. This chapter will provide a brief overview of
each component and how it relates to the wider Cwtch ecosystem.

## [openprivacy/connectivity](https://git.openprivacy.ca/openprivacy/connectivity) 

Summary: A library providing an ACN (Anonymous Communication Network ) networking abstraction.

The goal of connectivity is to abstract away the underlying libraries/software needed to communicate with a 
specific ACN. Right now we only support Tor and so the job of connectivity is to:

* Start and Stop the Tor Process
* Provide configuration to the Tor process
* Allow raw connections to endpoints via the Tor process (e.g. connect to onion services)
* Host endpoints via the Tor process (e.g. host onion services)
* Provide status updates about the underlying Tor process

For more information see [connectivity](./connectivity.md)

## [cwtch.im/tapir](https://git.openprivacy.ca/cwtch.im/tapir)

Summary: Tapir is a small library for building p2p applications over anonymous communication systems.

The goal of tapir is to abstract away **applications** over a particular ACN. Tapir supports:

* Creating a cryptographic identity (including ephemeral identities) 
* Maintaining a connection pool of inbound and outbound connections to services
* Handling various application-layers including cryptographic transcripts, [authentication and authorization protocols](https://docs.openprivacy.ca/cwtch-security-handbook/authentication_protocol.html#authentication-protocol), and
[token-based services via PrivacyPass](https://docs.openprivacy.ca/cwtch-security-handbook/tapir.html#token-app),

For more information see [tapir](./tapir.md)

## [cwtch.im/cwtch](https://git.openprivacy.ca/cwtch.im/cwtch)

Summary: Cwtch is the main library for implementing the cwtch protocol / system.

The goal of cwtch is to provide implementations for cwtch-specific applications e.g.
message sending, groups, and  file sharing(implemented as Tapir applications), provide interfaces for managing and storing Cwtch profiles, provide an event bus for subsystem splutting and building plugins with new functionality, in addition to managing other core functionality.

The cwtch library is also responsible for maintaining canonical model representations for wire formats and overlays.


## [cwtch.im/libcwtch-go](https://git.openprivacy.ca/cwtch.im/libcwtch-go)

Summary: libcwtch-go provides C (including Android) bindings for Cwtch for use in UI implementations.

The goal of libcwtch-go is to bridge the gap between the backend cwtch library and any front end systems which
may be written in a different language.

The API provided by libcwtch is much more restricted than the one provided by Cwtch directly, each libcwtch API typically
packages up several calls to Cwtch.

libcwtch-go is also responsible for managing UI settings and experimental gating. It is also often used as a staging ground
for experimental features and code that may eventually end up in Cwtch.

## [cwtch-ui](https://git.openprivacy.ca/cwtch.im/cwtch-ui)

Summary: A flutter based UI for Cwtch.

Cwtch UI uses libcwtch-go to provide a complete UI for Cwtch, allowing people to create and manage profiles,
add contacts and groups, message people, share files (coming soon) and more.

The UI is also responsible for managing localization and translations.

For more information see [Cwtch UI](./ui.md)

## Auxiliary Components

Occasionally, Open Privacy will factor out parts of Cwtch into standalone libraries that are not Cwtch specific.
These are briefly summarized here:

### [openprivacy/log](https://git.openprivacy.ca/openprivacy/log)

An Open Privacy specific logging framework that is used throughout Cwtch packages.