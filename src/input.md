# Input


## Risk: Interception of Cwtch content or metadata through an IME on Mobile Devices

**Status: Partially Mitigated**

Any component that has the potential to intercept data between a person, and the Cwtch app is a 
potential security risk.

One of the most likely interceptors is a 3rd party IME (Input Method Editor) commonly used
by people to generate characters not natively supported by their device.

Even benign and stock IME apps may unintentionally leak information about the contents of a persons message e.g.
through cloud synchronization, cloud translation or personal dictionaries.

Ultimately, this problem cannot be solved by Cwtch alone, and is a wider risk impacting the entire mobile
ecosystem.

A similar risk exists on desktop through the use of similar input applications (in addition to software keyloggers),
however we consider that fully outside the scope of Cwtch risk assessment (in line with other attacks on the security of the underlying 
operating system itself).

This is partially mitigated in Cwtch 1.2 through the use of `enableIMEPersonalizedLearning: false`. See
[this PR](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/142) for more information.