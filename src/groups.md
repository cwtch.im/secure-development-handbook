# Groups

For the most part the Cwtch risk model for groups is split into two distinct
profiles:

* Groups made up of mutually trusted participants where peers are assumed
 honest.
* Groups consisting of strangers where peers are assumed to be potentially
 malicious.
 
Most of the mitigations described in this section relate to the latter case, but
naturally also impact the former. Even if assumed honest peers later turn
 malicious there are mechanisms that can detect such malice and prevent it from
 happening in the future.
 
## Risk Overview: Key Derivation

In the ideal case we would use a protocol like OTR, the limitations preventing
us from doing so right now are:

* Offline messages are not guaranteed to reach all peers, and as such any
metadata relating to key material might get lost. We need a key derivation
 process which is robust to missing messages or incomplete broadcast. 
 
## Risk: Malicious Peer Leaks Group Key and/or Conversation

**Status: Partially Mitigated (but impossible to mitigate fully)**

Whether dealing with trusted smaller groups or partially-public larger groups
there is *always* the possibility that a malicious actor will leak group
messages.
  
We plan to make it easy for peers to [fork](#fork) groups to mitigate the
 same key being used to encrypt lots of sensitive information and provide
  some level of forward secrecy for past group conversations.

## Risk: Active Attacks by Group Members

**Status: Partially Mitigated**

Group members, who have access to the key material of the group, can conspire
 with a server or other group members to break transcript consistency.

While we cannot directly prevent censorship given this kind of active
 collusion, we have a number of mechanisms in place that should reveal the
 presence of censorship to honest members of the group. 
 
### Mitigations:
 
* Because each message is signed by the peers public key, it should not be
possible (within the cryptographic assumptions of the underlying cryptography)
for one group member to imitate another.
* Each message contains a unique identifier derived from the contents and the
previous message hash - making it impossible for collaborators to include
messages from non-colluding members without revealing an implicit message
chain (which if they were attempting to censor other messages would
 reveal such censorship)
 
Finally: We are actively working on adding non-repudiation to Cwtch servers such
that they themselves are restricted in what they can censor efficiently.
