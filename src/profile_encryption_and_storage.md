# Profile Encryption & Storage

Profiles are stored locally on disk and encrypted using a key derived from user-known password (via pbkdf2).

Note that once encrypted and stored on disk, the only way to recover a profile is by rederiving the password - as such
it isn't possible to provide a full list of profiles a user might have access to until they enter a password.

## Unencrypted Profiles and the Default Password

To handle profiles that are "unencrypted" (i.e don't require a password to open) we currently create a profile
with a [defacto, hardcoded password](https://git.openprivacy.ca/cwtch.im/libcwtch-go/src/branch/trunk/constants/globals.go#L5).

This isn't ideal, we would much rather wish to rely on OS-provided key material such that the profile is bound to a 
specific device, but such features are currently patchwork - we also note by creating an unencrypted profile, people
who use Cwtch are explicitly opting into the risk that someone with access to the file system may be able to decrypt
their profile.

### Related Code

https://git.openprivacy.ca/cwtch.im/cwtch/src/commit/3529e21b0e0763ca6ec20ddfd13e22c8070c4915/storage/v1/file_enc.go
